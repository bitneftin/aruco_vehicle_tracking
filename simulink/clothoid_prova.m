classdef (StrictDefaults)clothoid_prova < matlab.System & matlab.system.mixin.Propagates
%slexVarSizeMATLABSystemFindIfSysObj Find input elements that satisfy the given condition.
%     The condition is specified in the block dialog. Both outputs 
%     are variable-sized vectors.
%

%#codegen
% Copyright 2015 The MathWorks, Inc.

properties (Nontunable)
 
end

properties (Access = private)
   % The only property is the associated primitive
   S = ClothoidList();
   tmpo = ClothoidList();
end

properties (Constant, Hidden)

end

  % methods
  %   function this = slexVarSizeMATLABSystemFindIfSysObj(varargin)
  %       setProperties(this, nargin, varargin{:});
  %   end
  % end
  
  methods(Access=protected)
    
      function validatePropertiesImpl(obj)

      end

      function setupImpl(obj) % init fnct
          
        x0 = 100;
        y0 = 2;
        road_track = 2;
          
        P1_right = [x0 road_track pi];
        P2_right = [10 road_track pi];
        P5_right = [-road_track -10 -pi/2];
        P6_right = [-road_track -x0 -pi/2];
        kappa = 0;
        
        obj.tmpo.build_3arcG2(P1_right(1),P1_right(2), P1_right(3), kappa, P2_right(1),P2_right(2), P2_right(3), kappa) ; % track creation
        obj.S.append(obj.tmpo)
        obj.tmpo.build_3arcG2(P2_right(1),P2_right(2), P2_right(3), kappa, P5_right(1),P5_right(2), P5_right(3), kappa) ; % track creation
        obj.S.append(obj.tmpo)
        obj.tmpo.build_3arcG2(P5_right(1),P5_right(2), P5_right(3), kappa, P6_right(1),P6_right(2), P6_right(3), kappa) ; % track creation
        obj.S.append(obj.tmpo)

      end

      function [ output ] = stepImpl(obj, x, y )
          % Functions that returns the available time slots to cross the intersection
          L_preview = 3;
          % ~~~ dimensions variables ~~~~~~~~~
          [t, n] = obj.S.find_coord(x,y);
          [x_out, y_out, theta, k] = obj.S.evaluate(t+L_preview);
          if(theta < 0)
            theta = theta+2*pi;
          end
          theta = theta*180/pi;
          output = [x_out, y_out, theta];         
      end
    
    % function num = getNumOutputsImpl(~)
    %     num = 2;
    % end

   function [sz_1] = getOutputSizeImpl(obj) 
      sz_1 = [1 3]; % TEMPORARY !!! 
   end
    
    function [fz1] = isOutputFixedSizeImpl(~)
      %Both outputs are always variable-sized
      fz1 = true; % TEMPORARY !!! 
    end
    
    function [dt1] = getOutputDataTypeImpl(obj)
        dt1 = 'double';
    end
    
    function [cp1] = isOutputComplexImpl(obj)
        cp1 = false; % Linear indices are always real value
    end
    
  end
  
end