clear
[allData, scenario, sensor] = prova_scenario();

aruco1.x = zeros(1,length(allData));
aruco1.y = zeros(1,length(allData));
aruco1.z = zeros(1,length(allData));

aruco2.x = zeros(1,length(allData));
aruco2.y = zeros(1,length(allData));
aruco2.z = zeros(1,length(allData));

for i = 1:length(allData)
    x(i) =  allData(i).ActorPoses(1).Position(1);
    y(i) =  allData(i).ActorPoses(1).Position(2);
    
    x1(i) =  allData(i).ActorPoses(2).Position(1);
    y1(i) =  allData(i).ActorPoses(2).Position(2);
%   
    for j = 1:size(allData(i).ObjectDetections)
        if(isempty(allData(i).ObjectDetections{j,1}.ObjectClassID))
            return
        elseif (allData(i).ObjectDetections{j,1}.ObjectClassID == 4)
            aruco1.x(i) = allData(i).ObjectDetections{j,1}.Measurement(1);
            aruco1.y(i) = allData(i).ObjectDetections{j,1}.Measurement(2);
            aruco1.z(i) = allData(i).ObjectDetections{j,1}.Measurement(3);
        elseif(allData(i).ObjectDetections{j,1}.ObjectClassID == 3)
            aruco2.x(i) = allData(i).ObjectDetections{j,1}.Measurement(1);
            aruco2.y(i) = allData(i).ObjectDetections{j,1}.Measurement(2);
            aruco2.z(i) = allData(i).ObjectDetections{j,1}.Measurement(3);        
        end
    end
end

%%
T1 = makehgtform('translate',[50,-5,0],'zrotate',pi);
T2 = makehgtform('translate',[70, -5,0],'zrotate',pi);


for i = 1:length(aruco1.x)
    To1 = [1 0 0 aruco1.x(i);0 1 0 aruco1.y(i);0 0 1 aruco1.z(i);0 0 0 1];
    newT1(:,:,i) = T1*To1;
    To2 = [1 0 0 aruco2.x(i);0 1 0 aruco2.y(i);0 0 1 aruco2.z(i);0 0 0 1];
    newT2(:,:,i) = T2*To2;
end

newx(1,:) = newT1(1,4,:);
newy(1,:) = newT1(2,4,:);
newx1(1,:) = newT2(1,4,:);
newy1(1,:) = newT2(2,4,:);

    
figure()
plot(newx, newy,'+')
hold on 
plot(newx1, newy1,'+')
plot(x,y)

%% velocity and fake gps
dt = scenario.SampleTime;
for i = 1:(length(x)-1)
   Vx(i) = (((x(i+1)-x(i))/dt)); 
   Vy(i) = ((y(i+1)-y(i))/dt);
   V(i) = sqrt(Vx(i)^2+ Vy(i)^2);                 % absolute velocity    
end

x_gps = awgn(x, 2);
y_gps = awgn(y, 2);


figure()

hold on 
plot(x,y)
plot(x_gps, y_gps,'+')
axis equal

    

