clear all;

addpath(genpath('../Clothoids'));
% compile using "compile_lib"

% ~~~ initialize buses for actors in input ~~~~~~~~~
% refer to the tutorial:
% https://blogs.mathworks.com/simulink/2011/12/05/initializing-buses-using-a-matlab-structure/
actors.NumActors = 1;
actors.Time      = 1;
% poses
actors.Actors.ActorID      = 1;
actors.Actors.Position     = [ 4 4 4 ];
actors.Actors.Velocity     = [ 0 0 0 ];
%actor.Actors.Speed        = 0; % no if velocity is declared
actors.Actors.Roll         = 0;
actors.Actors.Pitch        = 0;
actors.Actors.Yaw          = 0;

busInfo = Simulink.Bus.createObject(actors);
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~